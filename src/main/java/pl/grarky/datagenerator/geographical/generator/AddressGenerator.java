package pl.grarky.datagenerator.geographical.generator;

import org.springframework.stereotype.Component;
import pl.grarky.datagenerator.geographical.data.Address;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@Component
public class AddressGenerator {

    public Address generateAddress() {
        return new Address(
                generateKey(),
                generateName(),
                generateFullName(),
                generateAirportCode()
        );
    }

    private String generateKey() {
        return generateAlphabeticString(10);
    }

    private String generateName() {
        return generateAlphabeticString(8);
    }

    private String generateFullName() {
        return generateAlphabeticString(24);
    }

    private String generateAirportCode() {
        return generateAlphabeticString(10);
    }

    private String generateAlphabeticString(int size) {
        return randomAlphabetic(size);
    }

}
