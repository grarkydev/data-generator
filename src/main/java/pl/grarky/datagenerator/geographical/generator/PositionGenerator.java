package pl.grarky.datagenerator.geographical.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.grarky.datagenerator.geographical.data.Position;

@Component
public class PositionGenerator {

    private final AddressGenerator addressGenerator;
    private final LocationGenerator locationGenerator;

    @Autowired
    public PositionGenerator(AddressGenerator addressGenerator, LocationGenerator locationGenerator) {
        this.addressGenerator = addressGenerator;
        this.locationGenerator = locationGenerator;
    }

    public Position generatePosition(long id) {
        return new Position(
                id,
                addressGenerator.generateAddress(),
                locationGenerator.generateLocation()
        );
    }

}
