package pl.grarky.datagenerator.geographical.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import pl.grarky.datagenerator.geographical.data.Position;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.LongStream.range;

@Controller
public class PositionsGenerator {

    private final PositionGenerator positionGenerator;

    @Autowired
    public PositionsGenerator(PositionGenerator positionGenerator) {
        this.positionGenerator = positionGenerator;
    }

    public List<Position> generatePositions(Long size) {
        return range(0, size)
                .mapToObj(positionGenerator::generatePosition)
                .collect(toList());
    }

}
