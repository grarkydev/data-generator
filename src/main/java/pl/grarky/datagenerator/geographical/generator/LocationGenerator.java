package pl.grarky.datagenerator.geographical.generator;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.springframework.stereotype.Component;
import pl.grarky.datagenerator.geographical.data.Coordinates;
import pl.grarky.datagenerator.geographical.data.Location;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_UP;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@Component
public class LocationGenerator {

    public Location generateLocation() {
        return new Location(
                generateCountry(),
                generateCoordinates(),
                generateLocationId(),
                generateIsInEurope(),
                generateCountryCode(),
                generateIsCoreCountry(),
                generateDistance()
        );
    }

    private String generateCountry() {
        return generateAlphabeticString(3);
    }

    private Coordinates generateCoordinates() {
        return new Coordinates(
                generateDoubleInRange(-90L, 90L),
                generateDoubleInRange(-180L, 180L)
        );
    }

    private Long generateLocationId() {
        return generateLongInRange(1L, 10000L);
    }

    private Boolean generateIsInEurope() {
        return generateBoolean();
    }

    private String generateCountryCode() {
        return generateAlphabeticString(2);
    }

    private Boolean generateIsCoreCountry() {
        return generateBoolean();
    }

    private BigDecimal generateDistance() {
        return valueOf(generateLongInRange(10L, 30000L));
    }

    private String generateAlphabeticString(int size) {
        return randomAlphabetic(size);
    }

    private Long generateLongInRange(long lower, long upper) {
        return new RandomDataGenerator().nextLong(lower, upper);
    }

    private Double generateDoubleInRange(double lower, double upper) {
        return valueOf(new RandomDataGenerator().nextUniform(lower, upper)).setScale(6, HALF_UP).doubleValue();
    }

    private Boolean generateBoolean() {
        return new Random().nextBoolean();
    }

}
