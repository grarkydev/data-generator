package pl.grarky.datagenerator.geographical.converter;

import org.springframework.stereotype.Controller;
import pl.grarky.datagenerator.geographical.data.GeographicPositionJson;
import pl.grarky.datagenerator.geographical.data.Position;
import pl.grarky.datagenerator.geographical.data.PositionJson;

@Controller
public class PositionConverter {

    public PositionJson toJson(Position position) {
        return new PositionJson(
                position.getId(),
                position.getAddress().getKey(),
                position.getAddress().getName(),
                position.getAddress().getFullName(),
                position.getAddress().getAirportCode(),
                position.getLocation().getCountry(),
                new GeographicPositionJson(
                        position.getLocation().getCoordinates().getLatitude(),
                        position.getLocation().getCoordinates().getLongitude()
                ),
                position.getLocation().getLocationId(),
                position.getLocation().getInEurope(),
                position.getLocation().getCountryCode(),
                position.getLocation().getCoreCountry(),
                position.getLocation().getDistance()
        );
    }

}
