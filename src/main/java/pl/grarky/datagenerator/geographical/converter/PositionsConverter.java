package pl.grarky.datagenerator.geographical.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import pl.grarky.datagenerator.geographical.data.Position;
import pl.grarky.datagenerator.geographical.data.PositionJson;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Controller
public class PositionsConverter {

    private final PositionConverter positionConverter;

    @Autowired
    public PositionsConverter(PositionConverter positionConverter) {
        this.positionConverter = positionConverter;
    }

    public List<PositionJson> toJson(List<Position> positions) {
        return positions.stream()
                .map(positionConverter::toJson)
                .collect(toList());
    }

}
