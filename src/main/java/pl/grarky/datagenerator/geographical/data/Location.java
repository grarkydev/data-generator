package pl.grarky.datagenerator.geographical.data;

import java.math.BigDecimal;

public class Location {

    private final String country;
    private final Coordinates coordinates;
    private final Long locationId;
    private final Boolean inEurope;
    private final String countryCode;
    private final Boolean coreCountry;
    private final BigDecimal distance;

    public Location(String country, Coordinates coordinates, Long locationId,
                    Boolean inEurope, String countryCode, Boolean coreCountry, BigDecimal distance) {
        this.country = country;
        this.coordinates = coordinates;
        this.locationId = locationId;
        this.inEurope = inEurope;
        this.countryCode = countryCode;
        this.coreCountry = coreCountry;
        this.distance = distance;
    }

    public String getCountry() {
        return country;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public Long getLocationId() {
        return locationId;
    }

    public Boolean getInEurope() {
        return inEurope;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public Boolean getCoreCountry() {
        return coreCountry;
    }

    public BigDecimal getDistance() {
        return distance;
    }
}
