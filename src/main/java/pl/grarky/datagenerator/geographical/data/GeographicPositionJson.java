package pl.grarky.datagenerator.geographical.data;

public class GeographicPositionJson {

    private final double latitude;
    private final double longitude;

    public GeographicPositionJson(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

}
