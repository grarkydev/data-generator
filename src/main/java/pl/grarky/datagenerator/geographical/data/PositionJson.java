package pl.grarky.datagenerator.geographical.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class PositionJson {

    @JsonProperty("_type")
    private final String entityType = "Position";

    @JsonProperty("_id")
    private final Long id;

    private final String key;

    private final String name;

    private final String fullName;

    @JsonProperty("iata_airport_code")
    private final String airportCode;

    @JsonProperty("type")
    private final String locationType = "location";

    private final String country;

    @JsonProperty("geo_position")
    private final GeographicPositionJson geographicPosition;

    @JsonProperty("location_id")
    private final Long locationId;

    private final Boolean inEurope;

    private final String countryCode;

    private final Boolean coreCountry;

    private final BigDecimal distance;

    public PositionJson(Long id, String key, String name, String fullName, String airportCode,
                        String country, GeographicPositionJson geographicPosition, Long locationId,
                        Boolean inEurope, String countryCode, Boolean coreCountry, BigDecimal distance) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.fullName = fullName;
        this.airportCode = airportCode;
        this.country = country;
        this.geographicPosition = geographicPosition;
        this.locationId = locationId;
        this.inEurope = inEurope;
        this.countryCode = countryCode;
        this.coreCountry = coreCountry;
        this.distance = distance;
    }

    public String getEntityType() {
        return entityType;
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public String getLocationType() {
        return locationType;
    }

    public String getCountry() {
        return country;
    }

    public GeographicPositionJson getGeographicPosition() {
        return geographicPosition;
    }

    public Long getLocationId() {
        return locationId;
    }

    public Boolean getInEurope() {
        return inEurope;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public Boolean getCoreCountry() {
        return coreCountry;
    }

    public BigDecimal getDistance() {
        return distance;
    }

}
