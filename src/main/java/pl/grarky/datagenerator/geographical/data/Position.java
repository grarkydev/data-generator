package pl.grarky.datagenerator.geographical.data;

public class Position {

    private final Long id;
    private final Address address;
    private final Location location;

    public Position(Long id, Address address, Location location) {
        this.id = id;
        this.address = address;
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public Address getAddress() {
        return address;
    }

    public Location getLocation() {
        return location;
    }
}
