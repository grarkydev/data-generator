package pl.grarky.datagenerator.geographical.data;

public class Address {

    private final String key;
    private final String name;
    private final String fullName;
    private final String airportCode;

    public Address(String key, String name, String fullName, String airportCode) {
        this.key = key;
        this.name = name;
        this.fullName = fullName;
        this.airportCode = airportCode;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAirportCode() {
        return airportCode;
    }
}
