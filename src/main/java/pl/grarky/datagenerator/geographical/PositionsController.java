package pl.grarky.datagenerator.geographical;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.grarky.datagenerator.geographical.data.PositionJson;
import pl.grarky.datagenerator.geographical.converter.PositionsConverter;
import pl.grarky.datagenerator.geographical.data.Position;
import pl.grarky.datagenerator.geographical.generator.PositionsGenerator;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
public class PositionsController {

    private final PositionsGenerator positionsGenerator;
    private final PositionsConverter positionsConverter;

    @Autowired
    public PositionsController(PositionsGenerator positionsGenerator, PositionsConverter positionsConverter) {
        this.positionsGenerator = positionsGenerator;
        this.positionsConverter = positionsConverter;
    }

    @GetMapping("/generate/json/{size}")
    public ResponseEntity<List<PositionJson>> generateGeographicalData(@PathVariable Long size) {
        return ok(jsonFrom(positionsWithSize(size)));
    }

    private List<Position> positionsWithSize(Long size) {
        return positionsGenerator.generatePositions(size);
    }

    private List<PositionJson> jsonFrom(List<Position> positions) {
        return positionsConverter.toJson(positions);
    }

}
