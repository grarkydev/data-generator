package pl.grarky.datagenerator.geographical;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import pl.grarky.datagenerator.DataGeneratorApplication;
import pl.grarky.datagenerator.geographical.converter.PositionsConverter;
import pl.grarky.datagenerator.geographical.data.PositionJson;
import pl.grarky.datagenerator.geographical.generator.PositionsGenerator;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
import static pl.grarky.datagenerator.geographical.data.PositionProvider.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DataGeneratorApplication.class)
@WebAppConfiguration
public class PositionsControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PositionsGenerator positionsGenerator;

    @Autowired
    private PositionsConverter positionsConverter;

    private PositionsController positionsController;

    @Before
    public void initialize() {
        positionsController = new PositionsController(positionsGenerator, positionsConverter);
        mockMvc = standaloneSetup(positionsController).build();
    }

    @Test
    public void shouldReturnGivenNumberOfPositionJsons() throws Exception {
        when(positionsGenerator.generatePositions(eq(3L)))
                .thenReturn(asList(warsaw(), london(), paris()));

        mockMvc.perform(get("/generate/json/3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    public void shouldReturnValidPositionJsonStructure() throws Exception {
        PositionJson warsawJson = warsawJson();

        when(positionsGenerator.generatePositions(eq(1L)))
                .thenReturn(singletonList(warsaw()));


        mockMvc.perform(get("/generate/json/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0]._type", equalTo(warsawJson.getEntityType())))
                .andExpect(jsonPath("$[0]._id", comparesEqualTo(warsawJson.getId().intValue())))
                .andExpect(jsonPath("$[0].key", equalTo(warsawJson.getKey())))
                .andExpect(jsonPath("$[0].name", equalTo(warsawJson.getName())))
                .andExpect(jsonPath("$[0].fullName", equalTo(warsawJson.getFullName())))
                .andExpect(jsonPath("$[0].iata_airport_code", equalTo(warsawJson.getAirportCode())))
                .andExpect(jsonPath("$[0].type", equalTo(warsawJson.getLocationType())))
                .andExpect(jsonPath("$[0].country", equalTo(warsawJson.getCountry())))
                .andExpect(jsonPath("$[0].geo_position.latitude", equalTo(warsawJson.getGeographicPosition().getLatitude())))
                .andExpect(jsonPath("$[0].geo_position.longitude", equalTo(warsawJson.getGeographicPosition().getLongitude())))
                .andExpect(jsonPath("$[0].location_id", comparesEqualTo(warsawJson.getLocationId().intValue())))
                .andExpect(jsonPath("$[0].inEurope", equalTo(warsawJson.getInEurope())))
                .andExpect(jsonPath("$[0].countryCode", equalTo(warsawJson.getCountryCode())))
                .andExpect(jsonPath("$[0].coreCountry", equalTo(warsawJson.getCoreCountry())))
                .andExpect(jsonPath("$[0].distance", comparesEqualTo(warsawJson.getDistance().doubleValue())));
    }

}
