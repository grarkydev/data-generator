package pl.grarky.datagenerator.geographical.data;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;

public class WarsawData {

    public static final Long warsawId = 192L;
    public static final String warsawKey = "Warsaw Key";
    public static final String warsawName = "Warsaw";
    public static final String warsawFullName = "Warsaw City";
    public static final String warsawAirportCode = "WAW";
    public static final String warsawCountry = "Poland";
    public static final Double warsawLatitude = 52.2297;
    public static final Double warsawLongitude = 21.0122;
    public static final Long warsawLocationId = 920L;
    public static final Boolean warsawInEurope = true;
    public static final String warsawCountryCode = "POL";
    public static final Boolean warsawCoreCountry = true;
    public static final BigDecimal warsawDistance = valueOf(92.18);

}
