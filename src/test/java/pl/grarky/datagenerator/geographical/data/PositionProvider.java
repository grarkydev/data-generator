package pl.grarky.datagenerator.geographical.data;

import java.math.BigDecimal;

import static pl.grarky.datagenerator.geographical.data.LondonData.*;
import static pl.grarky.datagenerator.geographical.data.ParisData.*;
import static pl.grarky.datagenerator.geographical.data.WarsawData.*;

public class PositionProvider {

    public static Position warsaw() {
        return positionFrom(warsawId, warsawKey, warsawName, warsawFullName, warsawAirportCode, warsawCountry, warsawLatitude,
                warsawLongitude, warsawLocationId, warsawInEurope, warsawCountryCode, warsawCoreCountry, warsawDistance
        );
    }

    public static PositionJson warsawJson() {
        return positionJsonFrom(warsawId, warsawKey, warsawName, warsawFullName, warsawAirportCode, warsawCountry, warsawLatitude,
                warsawLongitude, warsawLocationId, warsawInEurope, warsawCountryCode, warsawCoreCountry, warsawDistance
        );
    }

    public static Position london() {
        return positionFrom(londonId, londonKey, londonName, londonFullName, londonAirportCode, londonCountry, londonLatitude,
                londonLongitude, londonLocationId, londonInEurope, londonCountryCode, londonCoreCountry, londonDistance
        );
    }

    public static PositionJson londonJson() {
        return positionJsonFrom(londonId, londonKey, londonName, londonFullName, londonAirportCode, londonCountry, londonLatitude,
                londonLongitude, londonLocationId, londonInEurope, londonCountryCode, londonCoreCountry, londonDistance
        );
    }

    public static Position paris() {
        return positionFrom(parisId, parisKey, parisName, parisFullName, parisAirportCode, parisCountry, parisLatitude,
                parisLongitude, parisLocationId, parisInEurope, parisCountryCode, parisCoreCountry, parisDistance
        );
    }

    public static PositionJson parisJson() {
        return positionJsonFrom(parisId, parisKey, parisName, parisFullName, parisAirportCode, parisCountry, parisLatitude,
                parisLongitude, parisLocationId, parisInEurope, parisCountryCode, parisCoreCountry, parisDistance
        );
    }

    private static Position positionFrom(Long id, String key, String name, String fullName, String airportCode,
                                         String country, Double latitude, Double longitude, Long locationId,
                                         Boolean inEurope, String countryCode, Boolean coreCountry, BigDecimal distance) {
        return new Position(id,
                new Address(key, name, fullName, airportCode),
                new Location(country, new Coordinates(latitude, longitude),
                        locationId, inEurope, countryCode, coreCountry, distance)
        );
    }

    private static PositionJson positionJsonFrom(Long id, String key, String name, String fullName, String airportCode,
                                                 String country, Double latitude, Double longitude, Long locationId,
                                                 Boolean inEurope, String countryCode, Boolean coreCountry, BigDecimal distance) {
        return new PositionJson(id, key, name, fullName, airportCode, country,
                new GeographicPositionJson(latitude, longitude), locationId, inEurope, countryCode, coreCountry, distance);
    }

}
