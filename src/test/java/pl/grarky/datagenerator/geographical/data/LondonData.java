package pl.grarky.datagenerator.geographical.data;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;

public class LondonData {

    public static final Long londonId = 8271L;
    public static final String londonKey = "London Key";
    public static final String londonName = "London";
    public static final String londonFullName = "London City";
    public static final String londonAirportCode = "LON";
    public static final String londonCountry = "England";
    public static final Double londonLatitude = 51.5074;
    public static final Double londonLongitude = 0.1278;
    public static final Long londonLocationId = 472L;
    public static final Boolean londonInEurope = true;
    public static final String londonCountryCode = "ENG";
    public static final Boolean londonCoreCountry = false;
    public static final BigDecimal londonDistance = valueOf(422.49);

}
