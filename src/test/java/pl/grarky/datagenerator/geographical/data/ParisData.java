package pl.grarky.datagenerator.geographical.data;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;

public class ParisData {

    public static final Long parisId = 2421L;
    public static final String parisKey = "Paris Key";
    public static final String parisName = "Paris";
    public static final String parisFullName = "Paris City";
    public static final String parisAirportCode = "PAR";
    public static final String parisCountry = "France";
    public static final Double parisLatitude = 48.8566;
    public static final Double parisLongitude = 2.3522;
    public static final Long parisLocationId = 142L;
    public static final Boolean parisInEurope = true;
    public static final String parisCountryCode = "FRA";
    public static final Boolean parisCoreCountry = true;
    public static final BigDecimal parisDistance = valueOf(1142.53);

}
