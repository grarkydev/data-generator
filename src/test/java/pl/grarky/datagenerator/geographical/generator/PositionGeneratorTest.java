package pl.grarky.datagenerator.geographical.generator;

import org.junit.Test;
import pl.grarky.datagenerator.geographical.data.Position;

import static org.assertj.core.api.Assertions.assertThat;

public class PositionGeneratorTest {

    private final AddressGenerator addressGenerator = new AddressGenerator();
    private final LocationGenerator locationGenerator = new LocationGenerator();
    private final PositionGenerator positionGenerator = new PositionGenerator(addressGenerator, locationGenerator);

    @Test
    public void shouldGenerateValidPosition() {
        Position position = positionGenerator.generatePosition(1L);

        assertThat(position.getId()).isNotNull();
        assertThat(position.getAddress()).isNotNull();
        assertThat(position.getLocation()).isNotNull();
    }

    @Test
    public void shouldGeneratePositionWithGivenId() {
        Long id = 33L;

        Position position = positionGenerator.generatePosition(id);

        assertThat(position.getId()).isEqualTo(id);
    }

}
