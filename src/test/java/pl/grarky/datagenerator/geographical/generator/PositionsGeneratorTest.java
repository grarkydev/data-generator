package pl.grarky.datagenerator.geographical.generator;

import org.junit.Test;
import pl.grarky.datagenerator.geographical.data.Position;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PositionsGeneratorTest {

    private final AddressGenerator addressGenerator = new AddressGenerator();
    private final LocationGenerator locationGenerator = new LocationGenerator();
    private final PositionGenerator positionGenerator = new PositionGenerator(addressGenerator, locationGenerator);
    private final PositionsGenerator positionsGenerator = new PositionsGenerator(positionGenerator);

    @Test
    public void shouldGenerateListOfPositionsContainingGivenAmountOfElements() {
        Long positionsAmount = 12L;

        List<Position> positions = positionsGenerator.generatePositions(positionsAmount);

        assertThat(positions).hasSize(positionsAmount.intValue());
    }

}
