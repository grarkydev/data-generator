package pl.grarky.datagenerator.geographical.generator;

import org.junit.Test;
import pl.grarky.datagenerator.geographical.data.Location;

import static org.assertj.core.api.Assertions.assertThat;

public class LocationGeneratorTest {

    private final LocationGenerator locationGenerator = new LocationGenerator();

    @Test
    public void shouldGenerateValidLocation() {
        Location location = locationGenerator.generateLocation();

        assertThat(location.getCountry()).isNotBlank();
        assertThat(location.getCoordinates()).isNotNull();
        assertThat(location.getCoordinates().getLatitude()).isNotNull();
        assertThat(location.getCoordinates().getLongitude()).isNotNull();
        assertThat(location.getLocationId()).isNotNull();
        assertThat(location.getInEurope()).isNotNull();
        assertThat(location.getCountryCode()).isNotBlank();
        assertThat(location.getCoreCountry()).isNotNull();
        assertThat(location.getDistance()).isNotNull();
    }

}
