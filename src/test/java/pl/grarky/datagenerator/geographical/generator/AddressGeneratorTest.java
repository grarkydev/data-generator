package pl.grarky.datagenerator.geographical.generator;

import org.junit.Test;
import pl.grarky.datagenerator.geographical.data.Address;

import static org.assertj.core.api.Assertions.assertThat;

public class AddressGeneratorTest {

    private final AddressGenerator addressGenerator = new AddressGenerator();

    @Test
    public void shouldGenerateValidAddress() {
        Address address = addressGenerator.generateAddress();

        assertThat(address.getKey()).isNotBlank();
        assertThat(address.getName()).isNotBlank();
        assertThat(address.getFullName()).isNotBlank();
        assertThat(address.getAirportCode()).isNotBlank();
    }

}
