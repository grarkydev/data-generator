package pl.grarky.datagenerator.geographical.converter;

import org.junit.Test;
import pl.grarky.datagenerator.geographical.data.Position;
import pl.grarky.datagenerator.geographical.data.PositionJson;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.grarky.datagenerator.geographical.data.PositionProvider.*;

public class PositionConverterTest {

    private final PositionConverter positionConverter = new PositionConverter();

    @Test
    public void shouldCorrectlyConvertWarsawPositionToWarsawPositionJson() {
        assertThat(converted(warsaw())).isEqualToComparingFieldByFieldRecursively(warsawJson());
    }

    @Test
    public void shouldCorrectlyConvertParisPositionToParisPositionJson() {
        assertThat(converted(paris())).isEqualToComparingFieldByFieldRecursively(parisJson());
    }

    @Test
    public void shouldCorrectlyConvertLondonPositionToLondonPositionJson() {
        assertThat(converted(london())).isEqualToComparingFieldByFieldRecursively(londonJson());
    }

    private PositionJson converted(Position position) {
        return positionConverter.toJson(position);
    }

}
