package pl.grarky.datagenerator.geographical.converter;

import org.junit.Test;
import pl.grarky.datagenerator.geographical.data.Position;
import pl.grarky.datagenerator.geographical.data.PositionJson;

import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static pl.grarky.datagenerator.geographical.data.PositionProvider.*;

public class PositionsConverterTest {

    private final PositionConverter positionConverter = new PositionConverter();
    private final PositionsConverter positionsConverter = new PositionsConverter(positionConverter);

    @Test
    public void shouldCorrectlyConvertGivenListOfPositionsToListOfPositionJsons() {
        List<Position> givenPositions = asList(warsaw(), london(), paris());
        List<PositionJson> expectedPositionJsons = asList(warsawJson(), londonJson(), parisJson());

        assertThat(positionsConverter.toJson(givenPositions))
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyElementsOf(expectedPositionJsons);
    }

}
