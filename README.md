# Overview

## Main feature

Generating given amount of position data.

## Architecture

Service provides REST endpoint which allows to generate specified amount of position data.

# Data generation

## Schema

Only position data are generated right now.

Generated data format:
```json
[
    {
        "key": "JFhsAPJswm",
        "name": "CCXrdZHc",
        "fullName": "guwKHtkfZxeuCmxHfukuxntY",
        "country": "rsu",
        "inEurope": false,
        "countryCode": "pe",
        "coreCountry": true,
        "distance": 26802,
        "_type": "Position",
        "_id": 0,
        "iata_airport_code": "SNWQJNEMmt",
        "type": "location",
        "geo_position": {
            "latitude": 28.185708,
            "longitude": -118.385685
        },
        "location_id": 5507
    }
]
```

## Endpoint

`GET` request can be send to `/generate/json/{amount}` path to generate `position` data.

## Example request

Request:

`GET host:port/generate/json/3`

Result:
```json
[
    {
        "key": "XxpUVWRWIS",
        "name": "SQrTdytG",
        "fullName": "lgrUXoNlREuoxOlywgZGLECt",
        "country": "hNU",
        "inEurope": true,
        "countryCode": "Gd",
        "coreCountry": false,
        "distance": 29030,
        "_type": "Position",
        "_id": 0,
        "iata_airport_code": "hBVNvGnIAv",
        "type": "location",
        "geo_position": {
            "latitude": -73.339124,
            "longitude": 117.521289
        },
        "location_id": 9537
    },
    {
        "key": "ehAdnSLAqV",
        "name": "gxmYpWjT",
        "fullName": "YWKUlbgMcZsKaZHYqtVKAUZm",
        "country": "usw",
        "inEurope": true,
        "countryCode": "wZ",
        "coreCountry": false,
        "distance": 23132,
        "_type": "Position",
        "_id": 1,
        "iata_airport_code": "lypkWBVXXx",
        "type": "location",
        "geo_position": {
            "latitude": 76.338796,
            "longitude": 4.694463
        },
        "location_id": 3345
    },
    {
        "key": "GTYIUSKOWj",
        "name": "ZLirCrTr",
        "fullName": "dcZxrwOSAZySfYdxrMXntTsa",
        "country": "rGS",
        "inEurope": false,
        "countryCode": "uL",
        "coreCountry": true,
        "distance": 6779,
        "_type": "Position",
        "_id": 2,
        "iata_airport_code": "lYMEZsxRgS",
        "type": "location",
        "geo_position": {
            "latitude": -9.259789,
            "longitude": 117.715475
        },
        "location_id": 3933
    }
]
```

# Building

Service is build with Spring Boot as a Maven project, so can be build with a Maven command:

`mvn clean package`

# Running

Service can be launched with Maven command:

`mvn spring-boot:run`

Service by default starts on 8081 port (this can be changed in `application.yml` file).
